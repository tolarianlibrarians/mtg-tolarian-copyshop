﻿namespace Tolarian.Copyshop.Controller.ResponseObjects.Enums
{
    public enum CardType
    {
        Creature,
        Sorcery,
        Instant,
        Land,
        Planeswalker,
        Enchantment,
        Artifact,
        Unknown,
        Token,
        Emblem
    }
}