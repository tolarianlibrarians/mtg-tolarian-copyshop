﻿namespace Tolarian.Copyshop.Controller.ResponseObjects.Enums
{
    public enum RelatedCardType
    {
        token,
        meld_part,
        meld_result,
        combo_piece
    }
}