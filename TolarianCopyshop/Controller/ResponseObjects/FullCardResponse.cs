﻿using Tolarian.Copyshop.Controller.Interfaces;

namespace Tolarian.Copyshop.Controller.ResponseObjects
{
    public class FullCardResponse
    {
        public IFullCard Card { get; set; }
    }
}