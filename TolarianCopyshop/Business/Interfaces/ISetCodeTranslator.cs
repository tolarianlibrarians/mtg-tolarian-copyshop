﻿namespace Tolarian.Copyshop.Business.Interfaces
{
    public interface ISetCodeTranslator
    {
        string TranslateArenaCodeToScryfallCode(string setCode);
    }
}