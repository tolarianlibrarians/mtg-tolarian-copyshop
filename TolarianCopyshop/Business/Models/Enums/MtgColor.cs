﻿namespace Tolarian.Copyshop.Business.Models.Enums
{
    public enum MtgColor
    {
        /// <summary>
        /// Black
        /// </summary>
        B,
        /// <summary>
        /// Blue
        /// </summary>
        U,
        /// <summary>
        /// Red
        /// </summary>
        R,
        /// <summary>
        /// Green
        /// </summary>
        G,
        /// <summary>
        /// White
        /// </summary>
        W,
        /// <summary>
        /// Colorless
        /// </summary>
        C,
        /// <summary>
        /// Multicolor
        /// </summary>
        M,
    }
}