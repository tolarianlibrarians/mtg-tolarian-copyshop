﻿namespace Tolarian.Copyshop.Business.Models.Enums
{
    public enum CardImageTypes
    {
        Small,
        Normal,
        Large,
        Png,
        Art_Crop,
        Border_Crop
    }
}