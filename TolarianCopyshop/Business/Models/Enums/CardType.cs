﻿namespace Tolarian.Copyshop.Business.Models.Enums
{
    public enum CardType
    {
        Creature,
        Sorcery,
        Instant,
        Land,
        Planeswalker,
        Enchantment,
        Artifact,
        Unknown,
        Token,
        Emblem
    }
}