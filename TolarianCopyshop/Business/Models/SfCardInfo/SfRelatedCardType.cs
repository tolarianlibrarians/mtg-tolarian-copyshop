﻿namespace Tolarian.Copyshop.Business.Models.SfCardInfo
{
    public enum SfRelatedCardType
    {
        token,
        meld_part,
        meld_result,
        combo_piece
    }
}