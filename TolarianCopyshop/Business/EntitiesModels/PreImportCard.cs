﻿namespace Tolarian.Copyshop.Business.EntitiesModels
{
    public class PreImportCard
    {
        public string CardName { get; set; }
        public string SetCode { get; set; }
    }
}