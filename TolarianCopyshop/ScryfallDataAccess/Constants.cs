﻿namespace Tolarian.Copyshop.ScryfallDataAccess
{
    internal static class Constants
    {
        public const string SCRYFALL_BASE_URI = "https://api.scryfall.com";
    }
}